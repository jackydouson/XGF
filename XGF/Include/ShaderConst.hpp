/*
This header file is made by ShaderToCpp
DateTime:2017-08-21 20:35:32
*/

namespace ShaderConst{
extern const unsigned char fontVS[] ;
extern const unsigned int fontVSSize;
extern const unsigned char fontPS[] ;
extern const unsigned int fontPSSize;
extern const unsigned char shaderPTVS[] ;
extern const unsigned int shaderPTVSSize;
extern const unsigned char shaderPTPS[] ;
extern const unsigned int shaderPTPSSize;
extern const unsigned char shaderPCVS[] ;
extern const unsigned int shaderPCVSSize;
extern const unsigned char shaderPCPS[] ;
extern const unsigned int shaderPCPSSize;
}
